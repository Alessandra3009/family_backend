package App.Controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import App.Entities.Family;
import App.Services.FamilyService;

@RestController
public class FamilyController {
	
	@Autowired
	FamilyService familyService;
	
	@RequestMapping("/families")
	@CrossOrigin
	public List<Family> getFamilies(){
		return familyService.getFamilies();
	}
	
	@RequestMapping("/family/{id}")
	public Family getFamily(@PathVariable int id){
		return familyService.getFamily(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/createFamily")
	@CrossOrigin(origins = "*")
	public ResponseEntity<?> addFamily(@RequestBody Family family){
		familyService.addFamily(family);
		System.out.println("\nAdding the family");
		return new ResponseEntity<>(family,HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/updateFamily/{id}")
	public void updateFamily(@RequestBody Family family, @PathVariable int id){
		familyService.updateFamily(id, family);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/deleteFamily")
	public ResponseEntity<?> deleteFamily(@RequestBody Family family){
		familyService.deleteFamily(family);
		return new ResponseEntity<>("The family is deleted", HttpStatus.OK);
		
	}

}
