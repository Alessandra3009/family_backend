package App.Controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import App.Entities.Child;
import App.Services.ChildService;


@RestController
@ComponentScan(basePackages={"App.Services"})
public class ChildController {
	
	@Autowired
	ChildService childService;
	
	@RequestMapping("/children")
	@CrossOrigin
	public List<Child> getChildren(){
		return childService.getChildren();
	}
	
	@RequestMapping("/child/{id}")
	public Child getChild(@PathVariable int id){
		return childService.getChild(id);
	}
	
	@RequestMapping("/children/family-{idFamily}")
	@CrossOrigin
	public List<Child> getChildrenByFamily(@PathVariable int idFamily){
		return childService.getChildrenByFamilyIdFamily(idFamily);
	}	
	
	@RequestMapping(method=RequestMethod.POST, value="/createChild")
	@CrossOrigin(origins = "*")
	public ResponseEntity<?> addChild(@RequestBody Child child){
		childService.addChild(child);
		System.out.println("\nAdding the child");
		return new ResponseEntity<>(child,HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/updateChild/{id}")
	public void updateChild(@RequestBody Child child, @PathVariable int id){
		childService.updateChild(id, child);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/deleteChild")
	@CrossOrigin(origins="*")
	public ResponseEntity<?> deleteChild(@RequestBody Child child){
		childService.deleteChild(child);
		return new ResponseEntity<>("The child is deleted successfully", HttpStatus.OK);
	}
}
