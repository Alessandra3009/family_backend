package App.Controllers;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import App.Entities.Father;
import App.Entities.Sex;
import App.Services.FatherService;

@RestController
public class FatherController {
	
	@Autowired
	FatherService fatherService;
	
	@RequestMapping ("/fathers")
	@CrossOrigin
	public List<Father> getFathers(){
		return fatherService.getFathers();
	}
	
	@RequestMapping ("/father/{id}")
	public Father getFather(@PathVariable int id){
		return fatherService.getFather(id);
	}
	
	@RequestMapping("/fathers/child/firstName-{firstName}")
	@CrossOrigin
	public List<Father> getFathersByFamilyChildrenFirstName(@PathVariable String firstName){
		return fatherService.getFathersByFamilyChildrenFirstName(firstName);
	}
	
	@RequestMapping("/fathers/child/secondName-{secondName}")
	@CrossOrigin
	public List<Father> getFathersByFamilyChildrenSecondName(@PathVariable String secondName){
		return fatherService.getFathersByFamilyChildrenSecondName(secondName);
	}
	
	@RequestMapping("/fathers/child/pesel-{pesel}")
	@CrossOrigin
	public List<Father> getFathersByFamilyChildrenPesel(@PathVariable String pesel){
		return fatherService.getFathersByFamilyChildrenPesel(pesel);
	}
	
	@RequestMapping("/fathers/child/birthDate-{birthDate}")
	@CrossOrigin
	public List<Father> getFathersByFamilyChildrenBirthDate(@PathVariable @DateTimeFormat(pattern="yyyy-mm-dd")Date birthDate){
		return fatherService.getFathersByFamilyChildrenBirthDate(birthDate);
	}
	@RequestMapping("/fathers/child/sex-{sex}")
	@CrossOrigin
	public List<Father> getFathersByFamilyChildrenSex(@PathVariable Sex sex){
		return fatherService.getFathersByFamilyChildrenSex(sex);
	}
	
	@RequestMapping ("/father/family-{idFamily}")
	@CrossOrigin
	public Father getFatherByFamilyIdFamily(@PathVariable int idFamily){
		return fatherService.getFatherByFamilyIdFamily(idFamily);
	}
	
	@RequestMapping (method=RequestMethod.POST, value="/createFather")
	@CrossOrigin(origins = "*")
	public ResponseEntity<?> addFather(@RequestBody Father father){
		System.out.println("\nAdding the father");
		fatherService.addFather(father);
		return new ResponseEntity<>(father,HttpStatus.OK);
	}
	
	@RequestMapping (method=RequestMethod.PUT, value="/updateFather/{id}")
	public void updateFather(@RequestBody Father father, @PathVariable int id){
		fatherService.updateFather(id, father);
	}
	
	@RequestMapping (method=RequestMethod.POST, value="/deleteFather")
	public ResponseEntity<?> deleteFather(@RequestBody Father father){
		fatherService.deleteFather(father);
		return new ResponseEntity<>("The father is deleted", HttpStatus.OK);
	}

}
