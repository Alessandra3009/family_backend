package App.Repositories;

import org.springframework.data.repository.CrudRepository;
import App.Entities.Family;

public interface FamilyRepository extends CrudRepository <Family, Integer>{

}
