package App.Repositories;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import App.Entities.Child;

public interface ChildRepository extends CrudRepository <Child, Integer> {
	List <Child> findByFamilyIdFamily(int idFamily);

	

}
