package App.Repositories;

import java.util.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import App.Entities.Father;
import App.Entities.Sex;

public interface FatherRepository extends CrudRepository <Father, Integer> {
	List <Father> findDistinctByFamilyChildrenFirstName(String firstName);
	List <Father> findDistinctByFamilyChildrenSecondName(String secondName);
	List <Father> findByFamilyChildrenPesel(String pesel);
	List <Father> findDistinctByFamilyChildrenSex(Sex sex);
	List <Father> findDistinctByFamilyChildrenBirthDate(Date birthDate);
	Father findByFamilyIdFamily(int idFamily);

}
