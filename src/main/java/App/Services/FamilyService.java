package App.Services;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import App.Entities.Family;
import App.Repositories.FamilyRepository;

@Service
public class FamilyService {
	
	@Autowired
	FamilyRepository familyRepository;
	
	public List<Family> getFamilies(){
		List <Family> families= new ArrayList<Family>();
		familyRepository.findAll().forEach(families::add);
		return families;
	}
	
	public Family getFamily(int id){
		return familyRepository.findOne(id);
	}
	
	public Family addFamily(Family family){
		return familyRepository.save(family);
	}
	
	public Family updateFamily(int id, Family family){
		return familyRepository.save(family);
	}
	
	@Transactional
	public void deleteFamily(Family family){
		familyRepository.delete(family);
	}

}
