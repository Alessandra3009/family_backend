package App.Services;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import App.Entities.Child;
import App.Repositories.ChildRepository;

@Service
@ComponentScan(basePackages={"App.Repositories.ChildRepository"})
public class ChildService {
	
	@Autowired
	ChildRepository childRepository;
	
	public List<Child> getChildren(){
		List <Child> children= new ArrayList<Child>();
		childRepository.findAll().forEach(children::add);
		return children;
	}
	
	public Child getChild(int id){
		return childRepository.findOne(id);
	}
	
	 public List<Child> getChildrenByFamilyIdFamily(int idFamily){
		 List<Child> children= new ArrayList<>();
		 children.addAll(childRepository.findByFamilyIdFamily(idFamily));
		 return children;
	 }
	
	public Child addChild (Child child){
		return childRepository.save(child);
	}
	
	public Child updateChild(int id, Child child){
		return childRepository.save(child);
	}
	
	@Transactional
	public void deleteChild(Child child){
		childRepository.delete(child);
	}

}
