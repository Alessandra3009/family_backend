package App.Services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import App.Entities.Father;
import App.Entities.Sex;
import App.Repositories.FatherRepository;

@Service
public class FatherService {
	
	@Autowired
	FatherRepository fatherRepository;
	
	public List<Father> getFathers(){
		List<Father> fathers= new ArrayList<Father>();
		fatherRepository.findAll().forEach(fathers::add);
		return fathers;
	}
	
	public Father getFather(int id){
		return fatherRepository.findOne(id);
	}
	
	public List<Father> getFathersByFamilyChildrenFirstName(String firstName){
		 List<Father> fathers= new ArrayList<>();
		 fathers.addAll(fatherRepository.findDistinctByFamilyChildrenFirstName(firstName));
		 return fathers;
	 }
	
	public List<Father> getFathersByFamilyChildrenSecondName(String secondName){
		 List<Father> fathers= new ArrayList<>();
		 fathers.addAll(fatherRepository.findDistinctByFamilyChildrenSecondName(secondName));
		 return fathers;
	 }
	
	public List<Father> getFathersByFamilyChildrenPesel(String pesel){
		 List<Father> fathers= new ArrayList<>();
		 fathers.addAll(fatherRepository.findByFamilyChildrenPesel(pesel));
		 return fathers;
	 }
	
	public List<Father> getFathersByFamilyChildrenBirthDate(Date birthDate){
		 List<Father> fathers= new ArrayList<>();
		 fathers.addAll(fatherRepository.findDistinctByFamilyChildrenBirthDate(birthDate));
		 return fathers;
	 }
	
	public List<Father> getFathersByFamilyChildrenSex(Sex sex){
		 List<Father> fathers= new ArrayList<>();
		 fathers.addAll(fatherRepository.findDistinctByFamilyChildrenSex(sex));
		 return fathers;
	 }
	
	public Father getFatherByFamilyIdFamily(int idFamily){
		return fatherRepository.findByFamilyIdFamily(idFamily);
	 }
	
	public Father addFather(Father father){
		return fatherRepository.save(father);
	}
	
	public Father updateFather(int id, Father father){
		return fatherRepository.save(father);
	}
	
	@Transactional
	public void deleteFather(Father father){
		fatherRepository.delete(father);
	}
	
}
