package App;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaFamilyApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaFamilyApplication.class, args);
	}
}
