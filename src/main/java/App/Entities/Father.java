package App.Entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import org.springframework.format.annotation.DateTimeFormat;


@Entity
public class Father {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idFather;
	
	@Column(unique=false, nullable=false)
	private String firstName;
	
	@Column(unique=false, nullable=false)
	private String secondName;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Column(unique=false, nullable=false)
	private Date birthDate;
	
	@Column(unique=true, nullable=false)
	private String pesel;
	
	@OneToOne()
	@JoinColumn(name = "idFamily")
	private Family family;
	
	public Father(){}

	public Father(int idFather, String firstName, String secondName, Date birthDate, String pesel, Family family) {
		super();
		this.idFather = idFather;
		this.firstName = firstName;
		this.secondName = secondName;
		this.birthDate = birthDate;
		this.pesel = pesel;
		this.family = family;
	}

	public int getIdFather() {
		return idFather;
	}

	public void setIdFather(int idFather) {
		this.idFather = idFather;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}



	
}
