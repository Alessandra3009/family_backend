package App.Entities;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Family {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idFamily;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="family")
	@JsonIgnore
	private List<Child> children= new ArrayList<Child>();
	
	
	@OneToOne(fetch = FetchType.LAZY, 
    cascade =  CascadeType.ALL,
    mappedBy = "family") 
	@JsonIgnore
	private Father father;
	
	public Family(){}

	public Family(int idFamily, List<Child> children, Father father) {
		super();
		this.idFamily = idFamily;
		this.children = children;
		this.father = father;
	}

	public int getIdFamily() {
		return idFamily;
	}

	public void setIdFamily(int idFamily) {
		this.idFamily = idFamily;
	}

	public List<Child> getChildren() {
		return children;
	}

	public void setChildren(List<Child> children) {
		this.children = children;
	}

	public Father getFather() {
		return father;
	}

	public void setFather(Father father) {
		this.father = father;
	}

	
	
	

}
