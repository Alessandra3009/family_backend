package App.Entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Child {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idChild;
	
	@Column(unique=false, nullable=false)
	private String firstName;
	
	@Column(unique=false, nullable=false)
	private String secondName;
	
	@Column(unique=true, nullable=false)
	private String pesel;
	
	@Column(unique=false, nullable=false)
	@Enumerated(EnumType.STRING)
	private Sex sex;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Column(unique=false, nullable=false)
	private Date birthDate;
	
	@ManyToOne
	@JoinColumn(name = "idFamily")
	private Family family;

	public Child() {
	}

	public Child(int idChild, String firstName, String secondName, String pesel, Sex sex, Date birthDate,
			Family family) {
		super();
		this.idChild = idChild;
		this.firstName = firstName;
		this.secondName = secondName;
		this.pesel = pesel;
		this.sex = sex;
		this.birthDate = birthDate;
		this.family = family;
	}

	public int getIdChild() {
		return idChild;
	}

	public void setIdChild(int idChild) {
		this.idChild = idChild;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	
	
}
